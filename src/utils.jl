
function clust_res_per_amp(clusters)
    amp_to_clust = Dict(i => DataFrame(cluster=[],threshold=[]) for i in keys(clusters["sequences"]))
    for x in keys(clusters["clusters"])
        for y in clusters["clusters"][x]["amplicons"]
            push!(amp_to_clust[y], [x, clusters["clusters"][x]["threshold"]])
        end
    end
    return(amp_to_clust)
end

function get_parents!(clusters)
    amp_to_clust = clust_res_per_amp(clusters)
    parent_clusters = map( x -> sort(x, :threshold, rev = true)[1,:cluster],values(amp_to_clust)) |> unique
    filter!(p -> p.first ∈ parent_clusters,clusters["clusters"])
end