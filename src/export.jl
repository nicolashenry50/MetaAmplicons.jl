function get_cluster_table_unknowns(clusters)
    
    clust_table = DataFrame(
        cluster = [],
        threshold = [],
        nb_amplicons = [],
        parent_id = [],
        parent_references_LCA = [],
        parent_nb_references = []
    )

    for x in keys(clusters["clusters"])
        clust_table_to_add = [
            x,
            clusters["clusters"][x]["threshold"],
            length(clusters["clusters"][x]["amplicons"]),
            clusters["clusters"][x]["parent"],
            clusters["clusters"][x]["parent_references_LCA"],
            length(clusters["clusters"][x]["parent_references"])
        ]
        push!(clust_table, clust_table_to_add)
    end

    return(clust_table)

end

function get_cluster_table(clusters)

    clust_table = DataFrame(
        cluster=[],
        threshold=[],
        nb_amplicons=[],
        parent_id = [],
        references_LCA=[],
        references=[]
    )

    for x in keys(clusters["clusters"])
        clust_table_to_add = [
            x,
            clusters["clusters"][x]["threshold"],
            length(clusters["clusters"][x]["amplicons"]),
            clusters["clusters"][x]["parent"],
            clusters["clusters"][x]["references_LCA"],
            join(clusters["clusters"][x]["references"],",")
        ]
        push!(clust_table, clust_table_to_add)
    end
    return(clust_table)
end

function get_composition_table(clusters)
    clust_table = DataFrame(amplicon=[],cluster=[],threshold=[])
    for x in keys(clusters["clusters"])
        for y in clusters["clusters"][x]["amplicons"]
            push!(clust_table, [y, x, clusters["clusters"][x]["threshold"]])
        end
    end
    return(clust_table)
end

"""
    export_clust(clusters::AbstractDict; project_name::AbstractString = "my_project", output_dir::AbstractString = ".")

Write two files, a composition table listing the amplicons belonging to the clusters and a cluster table describing the clusters.

The composition table has three columns:

-   `amplicon`: amplicon id
-   `cluster`: cluster id
-   `threshold`: cluster single linkage threshold

The cluster table has five to six columns depending on the subset type (known or unknown):

-   `cluster`: cluster id
-   `threshold`: cluster single linkage threshold
-   `nb_amplicons`: number of amplicons composing the cluster
-   `parent_id`: cluster id of parent cluster (unknown only)
-   `references_LCA`: common taxonomy off all references placed in the cluster (known only)
-   `parent_references_LCA`: common taxonomy off all references placed in the parent cluster (unknown only)
-   `references`: ids of reference sequences associated to the cluster (known only)
-   `parent_nb_references`: number of reference sequences associated to the parent cluster (unknown only)

# Examples
```julia-repl
julia> export_clust(clusters_unknowns; project_name = "astan_unknowns", output_dir = ".")
```
"""
function export_clust(clusters::AbstractDict; project_name::AbstractString = "my_project", output_dir::AbstractString = ".")

    cluster_table_output = output_dir*"/"*project_name*".cluster_table.tsv.gz"
    composition_table_output = output_dir*"/"*project_name*".composition_table.tsv.gz"

    if clusters["taxo_sub"] == "unknowns"
        get_cluster_table_unknowns(clusters) |>
            CSV.write(cluster_table_output; compress = true, delim = "\t")
    else
        get_cluster_table(clusters) |>
            CSV.write(cluster_table_output; compress = true, delim = "\t")
    end

    get_composition_table(clusters) |>
        CSV.write(composition_table_output; compress = true, delim = "\t")

end