function prep_for_ref!(clusters)
    for x in keys(clusters["clusters"])
        clusters["clusters"][x]["references"] = []
    end
end


function add_references!(clusters, dists, refdb_name, header = true)

    clusters["refdb"] = refdb_name

    prep_for_ref!(clusters)

    amp_to_clust = clust_res_per_amp(clusters)

    stream =  CodecZlib.GzipDecompressorStream(open(dists, "r"))

    if header
        readline(stream)
        header = false
    end

    for line in eachline(stream)
        my_line = split(line, "\t") |> x -> String.(x)
        ref_id = my_line[1]
        amplicon_id = my_line[2]
        nuc_diff = parse(Int, my_line[3])
        if amplicon_id in keys(amp_to_clust)
            cluster_list = amp_to_clust[amplicon_id]
            cluster_list = cluster_list.cluster[cluster_list.threshold .>= nuc_diff]
            for x in cluster_list
                push!(clusters["clusters"][x]["references"], ref_id)
            end
        end
    end

    close(stream)
end

# add taxo, add references entry in clust object and add taxo (LCA to clust) and taxo to amplicons

function LCA(x,y)
    x = split(x, ";") |> x -> String.(x)
    y = split(y, ";") |> x -> String.(x)
    x[findall(map((t) -> t[1] == t[2], zip(x,y)))] |> x -> join(x,";")
end


function add_taxonomy!(clusters, taxo_file)

    refs_taxonomy = DataFrame(CSV.File(taxo_file; delim = "\t"))

    for x in keys(clusters["clusters"])
        refs_tmp = clusters["clusters"][x]["references"]
        if length(refs_tmp) > 0
            ref_id_index = indexin(refs_tmp, refs_taxonomy.ref_id)
            taxonomy_tmp = refs_taxonomy[ref_id_index,:taxonomy]
            if length(taxonomy_tmp) > 1
                taxonomy_tmp = reduce(LCA, taxonomy_tmp)
            else
                taxonomy_tmp = taxonomy_tmp[1]
            end
            clusters["clusters"][x]["references_LCA"] = taxonomy_tmp
        else
            clusters["clusters"][x]["references_LCA"] = ""
        end
    end
end


"""
    reference_placer(clusters::AbstractDict, dists::AbstractString, taxofile::AbstractString; refdb_name::AbstractString = "my_refdb", dists_header::Bool = true)

Place reference sequences and associated taxonomy (`taxofile::AbstractString`) in a cluster object (clusters::AbstractDict`) based on best hit results (`dists::AbstractString`).

# Examples
```julia-repl
julia> reference_placer(clusters, "data/refs.dist.gz", "data/46346_EukRibo-02_V4_2022-07-22.taxo.tsv"; refdb_name = "EukRibo_V4", dists_header = true)
```
"""
function reference_placer(clusters::AbstractDict, dists::AbstractString, taxofile::AbstractString; refdb_name::AbstractString = "my_refdb", dists_header::Bool = true)
    add_references!(clusters, dists, refdb_name, dists_header)
    add_taxonomy!(clusters, taxofile)
end

"""
    get_unknowns(clusters::AbstractDict; threshold::Int = 1)

Extracts the highest clusters in the hierarchy without references composed by at least `threshold::Int` amplicons.
Only works if reference sequences were placed using `reference_placer`

# Examples
```julia-repl
julia> get_unknowns(clusters)
```
"""
function get_unknowns(clusters::AbstractDict; threshold::Int = 1)
    clusters_unknowns = deepcopy(clusters)

    for x in keys(clusters_unknowns["clusters"])
        if (length(clusters_unknowns["clusters"][x]["references"]) > 0) | (clusters_unknowns["clusters"][x]["threshold"] < threshold)
            delete!(clusters_unknowns["clusters"], x)
        else
            parent_id = clusters_unknowns["clusters"][x]["parent"]
            if parent_id != ""
                clusters_unknowns["clusters"][x]["parent_references_LCA"] = clusters["clusters"][parent_id]["references_LCA"]
                clusters_unknowns["clusters"][x]["parent_references"] = clusters["clusters"][parent_id]["references"]
            else
                clusters_unknowns["clusters"][x]["parent_references_LCA"] = ""
                clusters_unknowns["clusters"][x]["parent_references"] = ""
            end
        end
    end

    sequences_left = map(x -> x["amplicons"],values(clusters_unknowns["clusters"])) |> 
        x -> reduce(vcat,x) |>
        unique

    for x in keys(clusters_unknowns["sequences"])
        if !(x in sequences_left)
            delete!(clusters_unknowns["sequences"], x)
        end
    end

    get_parents!(clusters_unknowns)

    clusters_unknowns["taxo_sub"] = "unknowns"

    return(clusters_unknowns)
end

"""
    get_knowns(clusters::AbstractDict, taxogroup::AbstractString)

Extracts the highest clusters in the hierarchy for which all the placed references taxonomy match `taxogroup::AbstractString`.
Only works if reference sequences were placed using `reference_placer`

# Examples
```julia-repl
julia> get_knowns(clusters, "Choanoflagellata")
```
"""
function get_knowns(clusters::AbstractDict, taxogroup::AbstractString)

    clusters_knowns = deepcopy(clusters)

    for x in keys(clusters_knowns["clusters"])
        if !occursin(Regex(taxogroup), clusters_knowns["clusters"][x]["references_LCA"])
            delete!(clusters_knowns["clusters"], x)
        end
    end
    
    sequences_left = map(x -> x["amplicons"],values(clusters_knowns["clusters"])) |> 
        x -> reduce(vcat,x) |>
        unique

    for x in keys(clusters_knowns["sequences"])
        if !(x in sequences_left)
            delete!(clusters_knowns["sequences"], x)
        end
    end

    get_parents!(clusters_knowns)

    clusters_knowns["taxo_sub"] = "knowns"

    return(clusters_knowns)

end