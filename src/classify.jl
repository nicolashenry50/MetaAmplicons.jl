"""
parse line from a dists file
sequence not already in the graph are added
using add_amplicon!()
"""
function line_parser!(my_line, amplicon_to_num, reverse_amplicon_to_num, graph)
    
    my_line = split(my_line, "\t") |> x -> String.(x)

        add_amplicon!(amplicon_to_num, reverse_amplicon_to_num, my_line[1], graph)
        add_amplicon!(amplicon_to_num, reverse_amplicon_to_num, my_line[2], graph)

    return (amplicon_to_num[my_line[1]], amplicon_to_num[my_line[2]], parse(Int,my_line[3]))

end

"""
take an sequence name, convert it to a integer using dict
and add it to the graph
"""
function add_amplicon!(amplicon_to_num, reverse_amplicon_to_num, amplicon, graph)

    if !haskey(amplicon_to_num, amplicon)
        amplicon_to_num[amplicon] = length(amplicon_to_num) + 1
        reverse_amplicon_to_num[amplicon_to_num[amplicon]] = amplicon
        LightGraphs.add_vertex!(graph)
    end

    return nothing

end

"""
add results of connected components
to out_cluster
"""
function save_clusters(out_cluster, graph_comp_new, reverse_amplicon_to_num, threshold)
    for i in eachindex(graph_comp_new)
        amplicons = get.([reverse_amplicon_to_num], graph_comp_new[i], "error")
        cluster_id = sort(amplicons) |> x -> join(x, "_") |> SHA.sha1 |> bytes2hex
        out_cluster[cluster_id] = Dict([("amplicons",amplicons),("threshold",threshold)])
    end
    return out_cluster
end

"""
place parents in a clust object
"""

function parent_placer!(cluster_object)

    amp_to_clust = clust_res_per_amp(cluster_object)

    for amp_lineage in values(amp_to_clust)

        amp_lineage_sorted = sort(amp_lineage, :threshold, rev = true)

        for (parent, child) in zip(amp_lineage_sorted[1:end-1,:cluster], amp_lineage_sorted[2:end,:cluster])
            cluster_object["clusters"][child]["parent"] = parent
        end

        cluster_object["clusters"][amp_lineage_sorted[1,:cluster]]["parent"] = ""

    end

end

"""
    get_clusters(dists::AbstractString; project_id::AbstractString = "my_project", header::Bool = true)

Create a cluster object from the gunziped tab separated file located at `dists::AbstractString`.

The file is composed by three columns, the first two columns are amplicons ids and the third contains the genetic distance (number of mutations).

# Examples
```julia-repl
julia> clusters = get_clusters("data/asvs.dist.gz"; project_id = "Astan", header = true)
Dict{String, Any} with 3 entries:
  "project"   => "Astan"
  "sequences" => Dict{String, Dict{Any, Any}}("ccdd3ea3ffd862750c5bf3858ce4209c1aaf979b"=>Dict(), "8a4c7ec9686f6e7f3c73306d49c6f68776f230d4"=>Dict(), "6d22adc04954da99f2aa6517e1f017e753a6cbd8"=>Dict(), "2dda22a9d4fd9634d9954b87171…
  "clusters"  => Dict{Any, Any}("f69684bad6c31853147af509b04d8f16c4361ff3"=>Dict{String, Any}("amplicons"=>["8cb054ea473edbbe39c6f322978e8e653efaef24"], "threshold"=>0, "parent"=>"23b30c5d0f69e069031800d656e80fd32ba8fba9"), "21333…
```
"""
function get_clusters(dists::AbstractString; project_id::AbstractString = "my_project", header::Bool = true)
    amplicon_to_num = Dict()
    reverse_amplicon_to_num = Dict()
    graph = LightGraphs.SimpleGraph()
    previous_clusters = Vector{Int64}()
    out_cluster = Dict()

    stream =  CodecZlib.GzipDecompressorStream(open(dists, "r"))

    if header
        readline(stream)
        header = false
    end

    # first edge with first line
    # define strating threshold
    parsed_line = line_parser!(readline(stream), amplicon_to_num, reverse_amplicon_to_num, graph)
    LightGraphs.add_edge!(graph, parsed_line[1], parsed_line[2])
    threshold = parsed_line[3]

    # for each line add edge to graph
    # if new threshold, snapshop clustering

    for line in eachline(stream)
        parsed_line = line_parser!(line, amplicon_to_num, reverse_amplicon_to_num, graph)
        if parsed_line[3] != threshold

            graph_mst = LightGraphs.SimpleGraph(size(graph)[1]) #Create a new graph
            for ew in LightGraphs.kruskal_mst(graph)
                LightGraphs.add_edge!(graph_mst,ew.src,ew.dst)
            end

            graph = graph_mst
            graph_comp = LightGraphs.connected_components(graph)
            graph_comp_new = setdiff(graph_comp,previous_clusters)
            previous_clusters = graph_comp
            out_cluster = save_clusters(out_cluster, graph_comp_new, reverse_amplicon_to_num, threshold)
            threshold = parsed_line[3]
        end
        LightGraphs.add_edge!(graph, parsed_line[1], parsed_line[2])
    end

    close(stream)

    graph_mst = LightGraphs.SimpleGraph(size(graph)[1]) #Create a new graph
    for ew in LightGraphs.kruskal_mst(graph)
        LightGraphs.add_edge!(graph_mst,ew.src,ew.dst)
    end
    graph = graph_mst
    graph_comp = LightGraphs.connected_components(graph)
    graph_comp_new = setdiff(graph_comp,previous_clusters)
    previous_clusters = graph_comp
    out_cluster = save_clusters(out_cluster, graph_comp_new, reverse_amplicon_to_num, threshold)
    
    out_cluster = Dict([("project",project_id),("clusters",out_cluster),("sequences",Dict(i => Dict() for i in keys(amplicon_to_num)))])

    # add amplicons as 0 clusters
    for amplicon in keys(amplicon_to_num)
        cluster_id = SHA.sha1(amplicon) |> bytes2hex
        out_cluster["clusters"][cluster_id] = Dict([("amplicons",[amplicon]),("threshold",0)])
    end

    parent_placer!(out_cluster)

    out_cluster["taxo_sub"] = "all"
    
    return out_cluster
end