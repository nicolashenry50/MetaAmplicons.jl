MetaAmplicons.jl
================

## Introduction

MetaAmplicons.jl is a Julia package that allows you to classify
amplicons (raw amplicons, ASVs or OTUs) based on their genetic distance
using a hierarchical single linkage clustering approach and extract
ecologically/taxonomically meaningful clusters. For now, the main
feature is the extraction of unknown clusters, the highest clusters in
the hierarchy only composed by amplicons distant from reference
sequences.

## Getting started in bash

### I thought it was a Julia package

Before using the package, you need to compute amplicon genetic pairwise
distances. That’s not something you can do for now using only
MetaAmplicons.jl. I detail below an example of how to compute genetic
distances, using `sumatra` and `vsearch`, with a rDNA 18SV4
metabarcoding dataset from the SOMLIT-Astan plankton time series
(Caracciolo et al. 2022).

### Download the data

A rDNA 18SV4 metabarcoding table (Swarm) from the SOMLIT-Astan plankton
time series is downloaded using `wget`. A rDNA 18SV4 reference database
(EukRibo) is also downloaded. It will be used to give a taxonomic
context to the classification results. Both metabarcoding table and
reference database are placed in a folder `data`.

``` bash
wget https://zenodo.org/record/5032451/files/ASTAN_18SV4_Swarm_v202009.tsv.gz -P data/
wget https://zenodo.org/record/6896896/files/46346_EukRibo-02_V4_2022-07-22.fas.gz -P data/
```

### Amplicon sequences

A fasta file is generated from the metabarcoding table. Only amplicons
with a total number of reads (column 3) superior to 100 are selected in
order to have a small fasta file to play with.

``` bash
zcat data/ASTAN_18SV4_Swarm_v202009.tsv.gz | \
    awk '$3 > 100 && NR > 1 {print ">"$1"\n"$5}' | \
    gzip > data/asvs.fasta.gz
```

### Amplicon sequence pairwise distances

Once you have your fasta file you can compute pairwise distances. I use
here
[sumatra](https://git.metabarcoding.org/obitools/sumatra/-/wikis/home)
to get raw score `-r` expressed as distance `-d` for pairs of amplicons
with a score below 30 `-t 30`. The resulting 3 columns table need to be
sorted by score for the MetaAmplicon.jl import function to work.

``` bash
(
    printf "query\ttarget\tnuc_diff\n"
    zcat data/asvs.fasta.gz | \
        sumatra -p 2 -d -r -t 60 - | \
        sed -r 's/\.0+//' | \
        sort --key 3,3n
) | \
    gzip > data/asvs.dist.gz
```

Here, the first 9 lines of the output table. The lowest score is 2.

|                  query                   |                  target                  | nuc_diff |
|:----------------------------------------:|:----------------------------------------:|:--------:|
| 0043c35c3a7616d621dbe07aa399505472eab685 | f10952fbb50ba8db2ff2fa87864d975e442934d1 |    2     |
| 00ebd7c3d4ba9b0f23041b27525a8b1ca50eff8b | f9ed4310e8afdd9d600b72f53901aea72d1573fa |    2     |
| 011003be1e9a8d6f3fada90dcb319cacf66e06e0 | 0e11c65a5b21ab64fe7137f6b0a61d7da9008395 |    2     |
| 01b076db7d8190fe1d29d52d6838a11b8ee87059 | e97d45d3e15cc1546769ab64c99da2e689a0e66e |    2     |
| 039f26fe261510b0b727a6bfa236977dee7c3243 | f633c3ea29bdc82d1a94ebfb87e049415bfedd48 |    2     |
| 046e9e2d79c3740b8b8f197349cbd22a96331d64 | c69e93d45626bd4e245be52b8633e187c02c729c |    2     |
| 04950e374a9a3daebdef19653ddf72431c8d1f5b | 3e532815127a89f99938228503e6bede727713f1 |    2     |
| 06cd5e247d7c493fe499bf7aadf1f12614b32f5e | c704de746f341f4d1f49bf8737d010794dabfbe0 |    2     |
| 09a41463d2cb1eb264a0fbd49f3baecef8e8b866 | 968e06adfad2ad3de453deda3ebbc6f2273c6a31 |    2     |

### Compare references with amplicons

The main function of MetaAmplicons.jl will classify your amplicons
(single linkage hierarchical clustering). You also have the possibility
to place reference sequences in this hierarchy to have a clue about the
taxonomic identity of the genetic clusters. To do so, you first need to
find the most similar amplicon of each reference sequence and the
corresponding number of differences. This is achieved here using
[vsearch](https://github.com/torognes/vsearch), looking for best hits
`top_hits_only` more than 80% similar `--id 0.80`. Vsearch does not
provide directly raw scores expressed as distances. That is computed,
however, as the difference between alignment lengths and numbers of
matches using `awk`.

``` bash
vsearch \
    --usearch_global data/46346_EukRibo-02_V4_2022-07-22.fas.gz \
    --db data/asvs.fasta.gz \
    --threads 8 \
    --id 0.80 \
    --iddef 1 \
    --maxaccepts 0 \
    --top_hits_only \
    --userfields query+target+ids+aln \
    --userout - | \
    awk '
        BEGIN{OFS = "\t"; print "query","target","nuc_diff"}
        {print $1, $2, length($4)-$3}
    '| \
    gzip > data/refs.dist.gz
```

Below an example of the kind of table you should have.

|  query   |                  target                  | nuc_diff |
|:--------:|:----------------------------------------:|:--------:|
| AB001038 | ca88f4a556864f48b5dbbe1aabcf3b9550f0b773 |    36    |
| AB001039 | ca88f4a556864f48b5dbbe1aabcf3b9550f0b773 |    37    |
| AB001108 | b342d49479e4626394c5f414cbd12aafd5b96d14 |    25    |
| AB001109 | b342d49479e4626394c5f414cbd12aafd5b96d14 |    25    |
| AB001111 | b342d49479e4626394c5f414cbd12aafd5b96d14 |    24    |
| AB001110 | b342d49479e4626394c5f414cbd12aafd5b96d14 |    24    |
| AB001112 | b342d49479e4626394c5f414cbd12aafd5b96d14 |    24    |
| AB001538 | fb76413612ab23ae9f6b3750bcd2307a528720eb |    31    |
| AB001538 | af0bd18ca7c1f6d90e51320e3af1ae23fc2e983c |    31    |

### Extract reference database taxonomy

Finally, we need a table linking the taxonomy to the reference id:

|  ref_id  |                                                                                  taxonomy                                                                                   |
|:--------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| AB000271 | Eukaryota;Diaphoretickes;Sar;Alveolata;Myzozoa;AC-clade;Apicomplexa;CM-group;coccidiomorphea;hematozoans;HP-clade;Piroplasmorida;Theileridae;g:Theileria;Theileria+sergenti |
| AB000638 |          Eukaryota;Amorphea;Obazoa;Opisthokonta;Nucletmycea;Fungi;core-fungi;Dikarya;Ascomycota;saccharomyceta;Saccharomycotina;g:Dipodascus;Dipodascus+ambrosiae           |
| AB000639 |         Eukaryota;Amorphea;Obazoa;Opisthokonta;Nucletmycea;Fungi;core-fungi;Dikarya;Ascomycota;saccharomyceta;Saccharomycotina;g:Dipodascus;Dipodascus+armillariae          |
| AB000640 |         Eukaryota;Amorphea;Obazoa;Opisthokonta;Nucletmycea;Fungi;core-fungi;Dikarya;Ascomycota;saccharomyceta;Saccharomycotina;g:Dipodascus;Dipodascus+macrosporus          |
| AB000641 |          Eukaryota;Amorphea;Obazoa;Opisthokonta;Nucletmycea;Fungi;core-fungi;Dikarya;Ascomycota;saccharomyceta;Saccharomycotina;g:Geotrichum;Geotrichum+klebahnii           |
| AB000642 |           Eukaryota;Amorphea;Obazoa;Opisthokonta;Nucletmycea;Fungi;core-fungi;Dikarya;Ascomycota;saccharomyceta;Saccharomycotina;g:Dipodascus;Dipodascus+albidus            |
| AB000643 |        Eukaryota;Amorphea;Obazoa;Opisthokonta;Nucletmycea;Fungi;core-fungi;Dikarya;Ascomycota;saccharomyceta;Saccharomycotina;g:Dipodascus;Dipodascus+australiensis         |
| AB000644 |         Eukaryota;Amorphea;Obazoa;Opisthokonta;Nucletmycea;Fungi;core-fungi;Dikarya;Ascomycota;saccharomyceta;Saccharomycotina;g:Dipodascus;Dipodascus+geniculatus          |
| AB000645 |          Eukaryota;Amorphea;Obazoa;Opisthokonta;Nucletmycea;Fungi;core-fungi;Dikarya;Ascomycota;saccharomyceta;Saccharomycotina;g:Dipodascus;Dipodascus+aggregatus          |

This table is built from the reference database fasta file:

``` bash
zcat data/46346_EukRibo-02_V4_2022-07-22.fas.gz | \
        awk '
            BEGIN{OFS = "\t"; print "ref_id","taxonomy"}
            /^>/{
                sub(/>/,"",$1)
                gsub(/\|/, ";", $2)
                print $1,$2
            }
        ' > data/46346_EukRibo-02_V4_2022-07-22.taxo.tsv
```

Now we have everything to use MetaAmplicons.jl :sunglasses:

## Getting started in Julia

### Installation

First of all, you need to install MetaAmplicons.jl.

In Julia:

``` julia
using Pkg
Pkg.add(url = "https://gitlab.com/nicolashenry50/MetaAmplicons.jl.git")
```

Once the package is installed, you can call it with:

``` julia
using MetaAmplicons
```

You can also load the package with `using` so you don’t have to type
`MetaAmplicons.` before each function. See
[here](https://docs.julialang.org/en/v1/manual/modules/#Standalone-using-and-import)
for more information about the difference between `import` and `using`.

### Create your cluster object

The function `get_clusters` classifies your amplicons using hierarchical
single linkage clustering. It can be seen as a hierarchical version of
[Swarm](https://github.com/torognes/swarm) (Mahé et al. 2021, 2014).

To use this function, you need to provide at least the path to the
amplicon pairwise distances file, see [related
section](#amplicon-sequence-pairwise-distances). By default, the
function expects to find column names in the file, if that is not the
case, specify it with `header = false`. You have also the possibility to
add the name of your project with the argument `project_id`.

``` julia
clusters = get_clusters(
    "data/asvs.dist.gz";
    project_id = "Astan",
    header = true
)
```

`get_clusters` returns a dictionary with three entries:

- **project**: project id
- **sequences**: a dictionary with amplicon ids as keys. No values
  associated to the keys for now
- **clusters**: a dictionary with cluster id as keys. Each cluster id is
  associated to a three entries dictionary:
  - **amplicons**: amplicons composing the cluster (amplicon ids array)
  - **threshold**: cluster single linkage threshold (number of
    differences). 0 means the sequences are identical
  - **parent**: parent cluster id, e.g. cluster with higher threshold
    containing the cluster

### Add taxonomic information

The function `reference_placer` places reference sequences and their
taxonomy into the amplicons hierarchy.

To use this function, you need to provide at least three arguments:

- cluster object generated by `get_clusters`
- path to references best hits file (gunziped tab seperated file; see
  [related section](#compare-references-to-amplicons))
- path to reference sequence taxonomy (tab seperated file; see [related
  section](#extract-reference-database-taxonomy))
- reference database name (`refdb_name`) (optional)
- if the distance file (second argument) contains column names or not
  (`dists_header`) (true by default)

``` julia
reference_placer(
    clusters,
    "data/refs.dist.gz",
    "data/46346_EukRibo-02_V4_2022-07-22.taxo.tsv";
    refdb_name = "EukRibo_V4",
    dists_header = true
)
```

The object clusters has now a fourth entry, `refdb`, giving the
reference database name.

Clusters are described by two extra entries

- `references`: array of ids of cluster associated reference sequences

- `references_LCA`: common taxonomy cluster associated reference
  sequences

### Taxonomic based extractions

Once the taxonomic information has been added, you can select clusters
based on their taxonomic identity. The function `get_knowns` extracts
the highest clusters in the hierarchy for which all the placed
references match a given taxonomy. Here an example with
Choanoflagellata:

``` julia
clusters_Choanoflagellata = get_knowns(clusters, "Choanoflagellata")
```

The function `get_unknowns` extracts the highest clusters in the
hierarchy without references:

``` julia
clusters_unknowns = get_unknowns(clusters)
```

### Export your cluster object

You can export your clusters as text files. The function `export_clust`
will write two files, a composition table listing the amplicons
belonging to the clusters and a cluster table describing the clusters.

The composition table has three columns:

- `amplicon`: amplicon id
- `cluster`: cluster id
- `threshold`: cluster single linkage threshold

The cluster table has five to six columns depending on the subset type
(known or unknown):

- `cluster`: cluster id
- `threshold`: cluster single linkage threshold
- `nb_amplicons`: number of amplicons composing the cluster
- `parent_id`: cluster id of parent cluster (unknown only)
- `references_LCA`: common taxonomy off all references placed in the
  cluster (known only)
- `parent_references_LCA`: common taxonomy off all references placed in
  the parent cluster (unknown only)
- `references`: ids of reference sequences associated to the cluster
  (known only)
- `parent_nb_references`: number of reference sequences associated to
  the parent cluster (unknown only)

``` julia
export_clust(clusters_unknowns; project_name = "astan_unknowns", output_dir = ".")
```

### References

<div id="refs" class="references csl-bib-body hanging-indent">

<div id="ref-Caracciolo2022" class="csl-entry">

Caracciolo, Mariarita, Fabienne Rigaut-Jalabert, Sarah Romac, Frédéric
Mahé, Samuel Forsans, Jean-Philippe Gac, Laure Arsenieff, et al. 2022.
“Seasonal Dynamics of Marine Protist Communities in Tidally Mixed
Coastal Waters.” *Molecular Ecology* 31 (14): 3761–83.
<https://doi.org/10.1111/mec.16539>.

</div>

<div id="ref-Mahé2021" class="csl-entry">

Mahé, Frédéric, Lucas Czech, Alexandros Stamatakis, Christopher Quince,
Colomban de Vargas, Micah Dunthorn, and Torbjørn Rognes. 2021. “Swarm
V3: Towards Tera-Scale Amplicon Clustering.” Edited by Inanc Birol.
*Bioinformatics* 38 (1): 267–69.
<https://doi.org/10.1093/bioinformatics/btab493>.

</div>

<div id="ref-Mahé2014" class="csl-entry">

Mahé, Frédéric, Torbjørn Rognes, Christopher Quince, Colomban de Vargas,
and Micah Dunthorn. 2014. “Swarm: Robust and Fast Clustering Method for
Amplicon-Based Studies.” *PeerJ* 2 (September): e593.
<https://doi.org/10.7717/peerj.593>.

</div>

</div>
